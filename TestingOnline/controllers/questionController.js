//  hiện thị đáp án user chọn

let sampleData = {
  id: 1,
  questionType: 1,
  content: "Đề văn được Đen tiết lộ trong MV mới là gì?",
  answers: [
    {
      id: "2",
      exact: false,
      content: "Người lái đò sông Đà",
      STT: null,
    },
    {
      id: "3",
      exact: false,
      content: "Người lái thuyền Sông Hương",
      STT: null,
    },
    {
      id: "4",
      exact: false,
      content: "Người vận chuyển",
      STT: null,
    },
    {
      id: "5",
      exact: true,
      content: "Đất Nước",
      STT: null,
    },
  ],
};
const renderRadioButtonAnser = (anser) => {
  return /*html*/ `<div class="form-check">
<label class="form-check-label">
  <input type="radio" class="form-check-input" name="singleChoice" value=${anser.id}>${anser.content}
</label>
</div>
`;
};

export const renderSingleQuestion = (question = sampleData) => {
  let ansersHTML = "";
  question.answers.forEach((item) => {
    ansersHTML += renderRadioButtonAnser(item);
  });

  // console.log(ansersHTML);

  let contentHTML = /*html*/ `<div>
   
  <p class="text-2xl ">Câu hỏi:  ${question.content}</p>

  <div class="grid grid-cols-2 gap-5 mt-10  ">
  ${ansersHTML}
  </div>
  
  </div> `;

  document.getElementById("quizContent").innerHTML = contentHTML;
};

export const renderFillInBlankQuestion = (question) => {
  let ansersHTML = `<input type="text"  class=" rounded p-3 text-blue-500" placeholder="Nhập đáp án"  name="singleChoice" />`;

  let contentHTML = /*html*/ `<div>
     
    <p class="text-2xl ">Câu hỏi:  ${question.content}</p>
  
    <div class="grid grid-cols-2 gap-5 mt-10  ">
    ${ansersHTML}
    </div>
    
    </div> `;

  document.getElementById("quizContent").innerHTML = contentHTML;
};
export const checkSingleChoice = (userAnserId, question) => {
  let index = question.answers.findIndex((item) => {
    // console.log(item);
    return item.id == userAnserId;
  });
  // console.log(index);
  // console.log(question.answers[index]);
  return question.answers[index].exact;
};
