import {
  checkSingleChoice,
  renderFillInBlankQuestion,
  renderSingleQuestion,
} from "./questionController.js";

let currentQuestionIndex = 0;
let score = 0;
let questionList = [];

// fetch("../data/questions.json")
//   .then((res) => {
//     // console.log(res.json());
//     res.json().then((res) => {
//       console.log(res);
//     });
//   })
//   .catch((err) => {
//     console.log(err);
//   });

// ẩn màn hình hiển thị đáp án
document.getElementById("quizResult").style.display = "none";

// promise chainning : khi gọi 2 hoặc nhiều api chạy theo thứ tự ( thằng đầu chạy xong thì chạy thằng tiếp theo)
fetch("../data/questions.json")
  .then((res) => {
    return res.json();
  })
  .then((res2) => {
    // console.log(res2);

    questionList = res2;
    // render lần đầu tiên
    renderSingleQuestion(res2[currentQuestionIndex]);

    let currentQuestionHTML = ` Câu ${currentQuestionIndex + 1} `;

    document.getElementById("currentIndex").innerHTML = currentQuestionHTML;
  })
  .catch((err) => {
    console.log(err);
  });

document.getElementById("nextQuestion").addEventListener("click", function () {
  //  data câu hỏi hiện tại
  let currentQuestion = questionList[currentQuestionIndex];

  if (currentQuestion.questionType == 1) {
    // xử lý single choice
    let userValue = document.querySelector(
      'input[name="singleChoice"]:checked'
    ).value;

    checkSingleChoice(userValue, currentQuestion) && score++;
  }
  console.log("câu hỏi hiện tại", currentQuestion);

  currentQuestionIndex++;

  // hiển thị nút nộp bài tập
  if (currentQuestionIndex === questionList.length - 1) {
    document.getElementById("nextQuestion").innerText = "Nộp bài";
  }
  // khi làm bài xong và nhấn vào nút hoàn thành
  if (currentQuestionIndex === questionList.length) {
    alert("Quá trình chấm điểm");
    return;
  }

  let currentQuestionHTML = ` Câu ${currentQuestionIndex + 1} `;

  document.getElementById("currentIndex").innerHTML = currentQuestionHTML;
  //  cập nhật lại câu hỏi mới
  currentQuestion = questionList[currentQuestionIndex];
  if (currentQuestion.questionType === 1) {
    renderSingleQuestion(currentQuestion);
  } else {
    renderFillInBlankQuestion(currentQuestion);
  }
});

answers = [
  {
    id: "2",
    exact: false,
    content: "Người lái đò sông Đà",
    STT: null,
  },
  {
    id: "3",
    exact: false,
    content: "Người lái thuyền Sông Hương",
    STT: null,
  },
  {
    id: "4",
    exact: false,
    content: "Người vận chuyển",
    STT: null,
  },
  {
    id: "5",
    exact: true,
    content: "Đất Nước",
    STT: null,
  },
];
